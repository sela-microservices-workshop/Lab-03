# Lab-03

# Microservices Workshop
Lab 03: Create CI pipelines with Azure DevOps

---


# Tasks

 - Create a build pipeline (for each service)
 
 - Add the "service name" and the "dockerhub user" as build variables
  
 - Configure "clean sources" for the build
  
 - Add the following steps to the build:

   * npm install
 
   * npm test

   * docker build
 
   * docker login
 
   * docker push

 - Enable continuous integration under "Triggers"
 
 - Update the build number format
 
 - Queue the build an ensure that was configured correctly



---


## Create a build pipeline (for each service)


- Browse to the main builds page and create a new build pipeline:

```
https://dev.azure.com/<your-organization>/microservices-workshop/_build
```

<img alt="Image 1.1" src="images/task-1.1.png"  width="700">
 
&nbsp;

- Select "Use the virtual designer":

<img alt="Image 1.2" src="images/task-1.2.png"  width="700">

&nbsp;

- Choose "Azure Repos Git" as source and select a service repository:

<img alt="Image 1.3" src="images/task-1.3.png"  width="700">

&nbsp;

- Select "Empty Job" and set the agent pool as "docker-agents":

<img alt="Image 1.4" src="images/task-1.4.png"  width="700">

&nbsp;

- Set "build-agents" as Agent pool and set the build definition name in the pipeline main configuration:

<img alt="Image 1.5" src="images/task-1.5.png"  width="700">

&nbsp;


## Add the "service name" and the "dockerhub user" as build variables


- Browse to variables tab and add the following variables:

```
SERVICE_NAME: <the-service-name>
DOCKERHUB_USERNAME: <your-dockerhub-user>
```

<img alt="Image 2.1" src="images/task-2.1.png"  width="700">
 
&nbsp;

## Configure "clean sources" for the build


- In the "Get Sources" section set the build to clean all directories:


<img alt="Image 3.1" src="images/task-3.1.png"  width="700">
 
&nbsp;

## Add the following steps to the build:


- Add a new task to the build:

<img alt="Image 4.1" src="images/task-4.1.png"  width="700">

&nbsp;

- Step 1: npm install
   
```
Task: npm
Display name: npm install
Command: install
```
   
<img alt="Image 4.2" src="images/task-4.2.png"  width="700">

&nbsp;

- Step 2: npm run test
   
```
Task: npm
Display name: npm test
Command: custom
Command and arguments: run test
```
   
<img alt="Image 4.3" src="images/task-4.3.png"  width="700">
    
&nbsp;

- Step 3: docker build
   
```
Task: docker
Display name: Docker Build
Command: build
Dockerfile: **/Dockerfile
Image name: $(DOCKERHUB_USERNAME)/$(SERVICE_NAME):$(Build.BuildId)
Include latest tag: Enabled
```
   
<img alt="Image 4.4" src="images/task-4.4.png"  width="700">
       
&nbsp;

- Step 4: docker login
   
```
Task: docker
Display name: Docker Login
Container registry type: Container Registry
Docker registry service connection: DockerHub
Command: login
```
   
<img alt="Image 4.5" src="images/task-4.5.png"  width="700">
     
&nbsp;

- Step 5: docker push
   
```
Task: docker
Display name: Docker Push
Container registry type: Container Registry
Docker registry service connection: DockerHub
Image name: $(DOCKERHUB_USERNAME)/$(SERVICE_NAME)
```
   
<img alt="Image 4.6" src="images/task-4.6.png"  width="700">
 
&nbsp;

## Enable continuous integration under "Triggers"

- Enable "continuous integration" for the branch master

<img alt="Image 5.1" src="images/task-5.1.png"  width="700">

&nbsp;

## Update the build number format under "Options"

- Set the build number format with the value:

```
$(SERVICE_NAME)-$(rev:r)
```

<img alt="Image 6.1" src="images/task-6.1.png"  width="700">

&nbsp;

## Save and queue the build to ensure that was configured correctly

- Repeat the process for each service


